package com.sree.nw.alarms;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.IteratorUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.flume.FlumeUtils;
import org.apache.spark.streaming.flume.SparkFlumeEvent;

import scala.Tuple2;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.sree.nw.alarms.Event;

public final class GroupEvents {

	private static KafkaProducer<String, String> producer = null;

	private static Logger logger = Logger.getLogger(GroupEvents.class);

	static Gson gson = new Gson();
	private static Properties props = new Properties();

	@SuppressWarnings("serial")
	public static void main(String[] args) throws Exception {

		props = new Properties();

		// Assign localhost id
		props.put("bootstrap.servers", "localhost:9092");

		// Set acknowledgements for producer requests.
		props.put("acks", "all");

		// If the request fails, the producer can automatically retry,
		props.put("retries", 0);

		// Specify buffer size in config
		props.put("batch.size", 16384);

		// Reduce the no of requests less than 0
		props.put("linger.ms", 1);

		// The buffer.memory controls the total amount of memory available to
		// the producer for buffering.
		props.put("buffer.memory", 33554432);

		props.put("key.serializer",
				"org.apache.kafka.common.serialization.StringSerializer");

		props.put("value.serializer",
				"org.apache.kafka.common.serialization.StringSerializer");

		producer = new KafkaProducer<String, String>(props);

		// Logger.getRootLogger().setLevel(Level.ERROR);

		// Duration batchInterval = new Duration(2000);
		SparkConf sparkConf = new SparkConf().setAppName("JavaFlumeEventCount")
		/* .setMaster("spark://172.16.207.143:7077"); */.setMaster("local[*]");

		JavaStreamingContext ssc = new JavaStreamingContext(sparkConf,
				Durations.seconds(10));
		JavaReceiverInputDStream<SparkFlumeEvent> flumeStream = FlumeUtils
				.createPollingStream(ssc, "localhost", 43333,
						StorageLevel.MEMORY_ONLY());

		PairFunction<SparkFlumeEvent, String, Event> pairFunction = new PairFunction<SparkFlumeEvent, String, Event>() {

			@Override
			public Tuple2<String, Event> call(SparkFlumeEvent flumeEvent)
					throws Exception {

				String val = new String(flumeEvent.event().getBody().array());
				logger.info(val);
				Event alarm = gson.fromJson(val, Event.class);
				return (extractKeyTuple(alarm));
			}
		};

		JavaPairDStream<String, Event> pairs = flumeStream
				.mapToPair(pairFunction);

		JavaPairDStream<String, Iterable<Event>> group = pairs.groupByKey();
		
		
		Function<Tuple2<String,Iterable<Event>>, Void> mapFunction = new Function<Tuple2<String,Iterable<Event>>, Void>() {

			@SuppressWarnings("unchecked")
			@Override
			public Void call(Tuple2<String, Iterable<Event>> v1) throws Exception {
				Iterator<Event> it = v1._2.iterator();
				List<Event> lst = IteratorUtils.toList(v1._2.iterator());
				JsonElement element = gson.toJsonTree(lst,new TypeToken<List<Event>>() {}.getType());
				logger.info(element.getAsJsonArray().toString());
				producer.send(new ProducerRecord<String, String>("alarm", element.getAsJsonArray().toString()));
				return null;
			}
		};
		
		group.map(mapFunction).print();

		/*group.foreachRDD(new VoidFunction<JavaPairRDD<String, Iterable<Event>>>() {
			@Override
			public void call(JavaPairRDD<String, Iterable<Event>> arg)
					throws Exception {

				JavaPairRDD<String, Iterable<Event>> pair = arg.wrapRDD(arg
						.rdd());
				logger.info(pair);
				if (pair.collectAsMap().size() > 0) {
					logger.info("+++++++++++++++++++++++++++++++");

					map = arg.collectAsMap();
					Set<String> keys = map.keySet();

					for (String key : keys) {
						logger.info(key + "\t" + map.get(key));
						// JsonElement element = gson.toJsonTree(map.get(key),
						// new TypeToken<Iterable<Event>>() {}.getType());
						// System.out.println(element.getAsJsonArray().toString());
						List<Event> lst = Lists.newArrayList(map.get(key));
						JsonElement element = gson.toJsonTree(lst,
								new TypeToken<List<Event>>() {
								}.getType());
						logger.info(element.getAsJsonArray().toString());
						//producer.send(new ProducerRecord<String, String>("alarm", element.getAsJsonArray().toString()));
					}
				}
			}
		});*/

		ssc.start();
		ssc.awaitTermination();
	}

	// Extract the key tuple out of transformed event
	private static Tuple2<String, Event> extractKeyTuple(Event alarm) {

		Tuple2<String, Event> keyTuple = new Tuple2<String, Event>(
				alarm.getFqn(), alarm);

		return keyTuple;
	}
}