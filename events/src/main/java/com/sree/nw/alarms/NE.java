package com.sree.nw.alarms;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class NE {

	@GraphId
	private Long id;
	private String name; 

	@Relationship(type = "EVENTS", direction = Relationship.OUTGOING)
	private List<Event> courses = new ArrayList<Event>();

	public List<Event> getCourses() {
		return courses;
	}

	public void setCourses(List<Event> courses) {
		this.courses = courses;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
