package com.sree.nw.alarms;

import java.io.Serializable;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@SuppressWarnings("serial")
@NodeEntity
public class Event  implements Serializable{

	@GraphId
	private Long id;

	String fqn;
	int alrmId;
	String cateory;
	String priority;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFqn() {
		return fqn;
	}

	public void setFqn(String fqn) {
		this.fqn = fqn;
	}

	public int getAlrmId() {
		return alrmId;
	}

	public void setAlrmId(int alrmId) {
		this.alrmId = alrmId;
	}

	public String getCateory() {
		return cateory;
	}

	public void setCateory(String cateory) {
		this.cateory = cateory;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alrmId == 0) ? 0 : alrmId+"".hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (((Event)obj).getAlrmId() == other.getAlrmId()) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", fqn=" + fqn + ", alrmId=" + alrmId
				+ ", cateory=" + cateory + ", priority=" + priority + "]";
	}
	
	
}
