package com.sree.nw.alarms;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.api.RpcClient;
import org.apache.flume.api.RpcClientFactory;
import org.apache.flume.event.EventBuilder;

import com.google.gson.Gson;

public class EventSender {

	public static void main(String[] args) {

		MyRpcClientFacade client = new MyRpcClientFacade();

		// Initialize client with the remote Flume agent's host and port
		client.init("localhost", 42222);

		client.sendDataToFlume();

		client.cleanUp();
	}
}

class MyRpcClientFacade {
	private RpcClient client;
	private String hostname;
	private int port;

	public void init(String hostname, int port) {
		// Setup the RPC connection
		this.hostname = hostname;
		this.port = port;
		this.client = RpcClientFactory.getDefaultInstance(hostname, port);
		// Use the following method to create a thrift client (instead of the
		// above line):
		// this.client = RpcClientFactory.getThriftInstance(hostname, port);
	}

	private List<Event> generateAlrms() {

		List<Event> list = new ArrayList<Event>(10);

		for (int i = 0; i < 10; i++) {
			Event a = new Event();
			a.setAlrmId(RandomUtils.nextInt());
			int random = RandomUtils.nextInt(3);
			a.setCateory(Constants.CATEGORY[random]);
			a.setFqn(Constants.FQN[random]);
			a.setPriority(Constants.PRIORITY[random]);

			list.add(a);
		}
		return list;
	}

	public void sendDataToFlume() {

		List<Event> alarms = generateAlrms();
		List<org.apache.flume.Event> events = new ArrayList<org.apache.flume.Event>(10);

		Gson gson = new Gson();

		for (Event a : alarms) {
			events.add(EventBuilder.withBody(gson.toJson(a), Charset.forName("UTF-8")));
			System.out.println(gson.toJson(a));

		}

		// Send the event
		try {
			client.appendBatch(events);
		} catch (EventDeliveryException e) {
			// clean up and recreate the client
			client.close();
			client = null;
			client = RpcClientFactory.getDefaultInstance(hostname, port);
		}
	}

	public void cleanUp() {
		// Close the RPC connection
		client.close();
	}

}