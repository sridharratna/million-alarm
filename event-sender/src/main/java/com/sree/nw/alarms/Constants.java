package com.sree.nw.alarms;

public class Constants {

	public static String[] FQN = {"ONE", "TWO", "THREE", "FOUR"};
	public static String[] PRIORITY = {"IGNORE", "MINOR", "MAJOR", "CRITICAL"};
	public static String[] CATEGORY = {"CATEGORY-1", "CATEGORY-2", "CATEGORY-3", "CATEGORY-4"};
}
