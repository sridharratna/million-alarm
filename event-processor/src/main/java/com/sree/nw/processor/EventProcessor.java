package com.sree.nw.processor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import kafka.serializer.StringDecoder;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.sree.nw.alarms.Event;

import scala.Tuple2;

public final class EventProcessor {

	private static Logger logger= Logger.getLogger(EventProcessor.class);
	
	@SuppressWarnings("serial")
	public static void main(String[] args) throws Exception {

		SparkConf sparkConf = new SparkConf().setAppName("FMXAlarmProcessor")
				.setMaster("local");
		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf,
				Durations.milliseconds(1000));

		Set<String> topicsSet = new HashSet<String>();
		topicsSet.add("alarm");

		Map<String, String> kafkaParams = new HashMap<String, String>();
		//kafkaParams.put("zookeeper.connect", "127.0.0.1:2181");
		kafkaParams.put("metadata.broker.list", "127.0.0.1:9092");
		
		// Create direct kafka stream with brokers and topics
		JavaPairInputDStream<String, String> alarms = KafkaUtils
				.createDirectStream(jssc, String.class, String.class,
						StringDecoder.class, StringDecoder.class, kafkaParams,
						topicsSet);
		
		JavaDStream<Map<String, Event[]>> lines = alarms
				.map(new Function<Tuple2<String, String>, Map<String, Event[]>>() {
					@Override
					public Map<String, Event[]> call(
							Tuple2<String, String> tuple2) {
						BufferedWriter br = null;
						try {
							logger.info(tuple2._1+"\t"+tuple2._2);
							br = new BufferedWriter(new FileWriter(new File("D:/tmp/mylog.log"),true));
							br.write(tuple2._1+"\t"+tuple2._2);
						} catch (IOException e) {
							e.printStackTrace();
							logger.error(e);
						}finally{
							try {
								br.close();
							} catch (IOException e) {
								e.printStackTrace();
								logger.error(e);
							}
						}
						return null;
					}
				});

		lines.print();

		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}
}